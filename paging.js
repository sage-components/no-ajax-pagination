/* eslint-disable */
transforms: { forOf: false }

jQuery(function($) {
  /*
  Include guide for pagination pairing:
    @include('partials.component-pagination', ['controller_id' => 'id'])
    'id' should be any string. That string should match 'id' in:
    @include('partials.component-{product/items/categ.}-list', ['controller_id' => 'id'])
  */
  /*
  * IN -> .pagination-wraper data options:
  *   *data-posts    : number of postst shown per page
  *    data-ifempty  : string to show when there is no items to show
  *   *data-for      : id of paired content with controller {{$controller_id}}
  *   mandatory are marked by *
  * 
  * example link:
  * <a href="javascript:void(0)" data-page="prev" class="prev" title="">
  * <a href="javascript:void(0)" data-page="next" class="prev" title="">
  * <a href="javascript:void(0)" data-page="4" class="prev" title="">
  *  hint: use javascript:void(0) instead #, to prevent scroll top where .off doesnt work
  */

  let global_pages = [];
  var ua = navigator.userAgent;
  var event_click = (ua.match(/iPad/i) || ua.match(/iPhone/)) ? "touchstart" : "click";

  /**
   * Ajax attributes changed -> reload pagination
   */
  $( document ).ajaxSuccess(function(){
    $('[name="products_per_page"][data-id]').each(
      function(){
        $(this).change({id: $(this).data("id")},change_handler).trigger('change');
      });
  });

  /**
   * Products Quantity per page select change event
   */
  $('[name="products_per_page"][data-id]').each(
    function(){
      $(this).change({id: $(this).data("id")},change_handler).trigger('change');
    });

  /**
   * Slectboxes
   */
  $('select.filter').change(function() {
    // set the window's location property to the value of the option the user has selected
    if( $(this).val() !== '' )
    window.location = $(this).val();
  }); 

  /**
   * Refresh view
   * If any attribute for pagination is changed,this function should be called
   */
  function change_handler(event){
    if($('[name="products_per_page"]:focus').data("id") === undefined){
      pair  = event.data.id;
    }else{
      pair  = $('[name="products_per_page"]:focus').data("id");
    }
    scope = page_attr(pair);

    //Display page elements (aantal/numbers) based attributes
    if( scope.posts_quan <= scope.per_page || scope.per_page === true) {
      //all / posts less than page scope
      $('ul[data-id="'+pair+'"]').parent('div.paginator-container').css("display", "none");
      $('div.page-amount-container[data-id="'+pair+'"]').css("display", "inline-block");
      if( scope.posts_quan <= 8 ){
        //less/equal than 8 posts
        $('div.page-amount-container[data-id="'+pair+'"]').css("display", "none");
        if (scope.posts_quan == "" || scope.posts_quan == 0 || scope.posts_quan == undefined) {
          //no posts
          $('div.pagination-wraper[data-id="'+pair+'"]').html('<p class="no-items">'+scope.if_empty_text+"</p>");
        }
      }
    }else{
      $('ul[data-id="'+pair+'"]').parent('div.paginator-container').css("display", "inline-block");
      $('div.page-amount-container[data-id="'+pair+'"]').css("display", "inline-block");
    }

    //Change book to 1st page (init state)
    change_list(pair, 0, scope.max_page);
    set_current_page(pair, 0);
    $('ul.paginator[data-id="'+pair+'"] li a').off(event_click).on(event_click, {id:pair}, click_handler);
    $('.pagination-wraper[data-id="'+pair+'"] div.page.block-shown').removeClass('block-shown').addClass('block-hidden').css("display", "none");
    $('.pagination-wraper[data-id="'+pair+'"] .item-'+scope.per_page+'-' + 0).addClass('block-shown').removeClass('block-hidden').css("display", "block");
    show_book(pair, scope.per_page);
  }

  // Helper functions for above

  /**
   * Show content div(book), based on products per page
   * @param {int} per_page 
   */
  function show_book(pair, per_page){
    switch(per_page) {
      case 8:
        show_content_div(pair, ".div-8");
        break;
      case 12:
        show_content_div(pair, ".div-12");
        break;
      case 16:
        show_content_div(pair, ".div-16");
        break;
      case true:
        show_content_div(pair, ".show_all_products");
        break;
    }
  }
  /**
   * Toggle div_id for pair of clicked controller
   * @param {*} pair 
   * @param {*} div_id 
   */
  function show_content_div(pair, div_id){
    //hide_all_content_divs
    $('.pagination-wraper[data-id="'+pair+'"] .div-8').removeClass('block-shown').addClass('block-hidden').css("display", "none");
    $('.pagination-wraper[data-id="'+pair+'"] .div-12').removeClass('block-shown').addClass('block-hidden').css("display", "none");
    $('.pagination-wraper[data-id="'+pair+'"] .div-16').removeClass('block-shown').addClass('block-hidden').css("display", "none");
    $('.pagination-wraper[data-id="'+pair+'"] .show_all_products').removeClass('block-shown').addClass('block-hidden').css("display", "none");
    //show the one from args
    $('.pagination-wraper[data-id="'+pair+'"] '+ div_id).removeClass('block-hidden').addClass('block-shown').css("display", "block");
  }

  /**
   * @param {string} pair - id
   * @param {Integer} page2go - page which have to show
   * @param {jQuery effect string} effect - 'slide'
   * @param {Integer} duration - default 500
   */
  function show_page(pair, page2go, effect, duration){
    scope = page_attr(pair);
      if(scope.current_page < page2go){ // left -> right
        direct_hide  = 'left';
        direct_show = 'right'; 
      }else{ //right -> left
        direct_hide  = 'right';
        direct_show = 'left';
      }
      //disable every link to avoid toggle of show/hide - enable on animation end
      $('ul.paginator[data-id='+pair+'] li a').off(event_click);
      //hide current page
      $(".pagination-wraper[data-id="+pair+"] div.page.block-shown")
          .clearQueue().finish()
          .hide({
            effect    : effect, 
            direction : direct_hide,
            queue     : false,
            duration  : duration,
            complete  : function(){
              $(this)
              .removeClass('block-shown')
              .addClass('block-hidden')
              .css("display", "none");
            }
      });

      //show new page
      $(".pagination-wraper[data-id="+pair+"] div.item-"+$('[name="products_per_page"][data-id='+pair+']').val() +'-' + page2go)
          .clearQueue().finish()
          .show({ 
            effect      : effect,
            direction   : direct_show,
            queue       : false,
            duration    : duration,
            complete    : function(){
              $(this)
              .removeClass('block-hidden')
              .addClass('block-shown')
              .css("display", "block");
              $('ul.paginator[data-id='+pair+'] li a').off(event_click).on(event_click, {id:pair}, click_handler);
            }
        });
      //Placeholder makes absolute div for relative coorinates,
      // By calling 2 animations simoultaneously, there are 2 divs, misplaced
      // probably on next layer, by removing one placeholder the effect
      // should be same and jQuery should animate, but the height/width is
      // deleted. The second ui-effect-placeholder should make this work
      // for first as well. 
      $.effects.removePlaceholder($('[data-id="'+pair+'"] div.item-'+$('[name="products_per_page"][data-id="'+pair+'"]').val() +'-' + page2go));
  }

  /**
   * change list numbers (+ current) for actual book
   * @param {string} pair -id
   * @param {int} curr - what page to set as current
   * @param {int} max - last page number
   */
  function change_list(pair, curr, max){
    $('ul.paginator[data-id="'+pair+'"] :not(:first-child,:last-child)').remove();
    $('ul.paginator[data-id="'+pair+'"] li:first-child').after(pagination(curr,max));
    //set current
    $('ul.paginator[data-id="'+pair+'"] li a.pagenumber').removeClass('current');
    $('ul.paginator[data-id="'+pair+'"] li a.pagenumber[data-page="'+ curr +'"]').addClass('current');
  }

  /**
   * changed source: https://gist.github.com/kottenator/9d936eb3e4e3c3e02598
   * @param {int} c current page
   * @param {int} m max page 
   */
  function pagination(c, m) {
    var current = c,
        last = m - 1,
        delta = 1,
        left = current - delta,
        right = current + delta + 1,
        range = [],
        rangeWithDots = [],
        l;
    
    for (let i = 0; i <= last; i++) {
        if (i == last || i >= left && i < right) {
          range.push(i);
        }
        // When iam on last page show 4 items
        if(current == last && (i == last-2 || i == last-3)){
          range.push(i);
        }
        // When iam on last-1 page show 4 items
        if(current == last-1 && i == last-3){
          range.push(i);
        }
    }
    for(let i in range) {
        if (l) {
            if (range[i] - l === 2) {
                rangeWithDots.push('<li>...</li>');
            } else if (range[i] - l !== 1) {
                rangeWithDots.push('<li>...</li>');
            }
        }
        rangeWithDots.push('<li><a href="javascript:void(0)" data-page="'+range[i]+'" class="pagenumber">'+ (range[i]+1) +'</a></li>');
        l = range[i];
    }
    return rangeWithDots;
  }

  /**
   * Paginator buttons functionality
   */
  function click_handler(event){
    event.preventDefault(); // Don't use link in href...
    pair = event.data.id;
    page2go = $(this).data('page');
    scope = page_attr(pair);

    if(Number.isInteger(page2go)){
        show_page(pair, page2go, 'slide', 500 );
        change_list(pair, page2go, scope.max_page);
        set_current_page(pair, page2go);
    }else if(page2go == "prev"){
      if( scope.current_page != 0 ){
        move_to = scope.current_page-1;
        show_page(pair, move_to, 'slide', 500 );
        change_list(pair, move_to, scope.max_page);
        set_current_page(pair, move_to);
      }
    }else if(page2go == "next"){
      if( scope.current_page != (scope.max_page-1) ){
        move_to = scope.current_page+1;
        show_page(pair, move_to, 'slide', 500 );
        change_list(pair, move_to, scope.max_page);
        set_current_page(pair, move_to);
      }
    }
  }

  /**
   * @returns object of attributes for book with id pair
   * @param {string} pair - id
   */
  function page_attr(pair){
    posts_quan  = $('.pagination-wraper[data-id="'+pair+'"]').data("posts");
    per_page    = $('[name="products_per_page"][data-id="'+pair+'"]').val();
    
    obj = {
      'posts_quan'    : posts_quan,
      'if_empty_text' : $('.pagination-wraper[data-id="'+pair+'"]').data("ifempty"),
      'per_page'      : isNaN(per_page) || parseInt(per_page),
      'max_page'      : Math.ceil(posts_quan / per_page),
      'pair'          : pair,
      'current_page'  : get_current_page(pair)
      }
      function get_current_page(pair){
        var result = global_pages.find( obj => obj.pair === pair );
        if(result == undefined){
          return 0;      
        } 
        return result.current_page;
      }

      index = global_pages.findIndex(obj => obj.pair === pair);
      if( index == -1 ){
        global_pages.push(obj);
      }else{
        global_pages[index] = obj;
      }
      return obj;
  }

  function set_current_page(pair, curr){
    global_pages[global_pages.findIndex(obj => obj.pair === pair)].current_page = curr;
  }
});

