<ul class="paginator" data-id="{{$controller_id}}">
    <li>
        <a href="javascript:void(0)" data-page="prev" class="prev" title="">
            <img src="@asset('images/arrow-left-white.svg')" />
        </a>
    </li>
    <li>
        <a href="javascript:void(0)" data-page="next" class="next" title="">
            <img src="@asset('images/arrow-right-white.svg')" />
        </a>
    </li>
</ul>
