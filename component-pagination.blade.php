{{-- 
    Blade attributes: 
        'controller_id' => string to identify a relationship with pagination controller numbers and pagianted content
        'show_header' => (isset) show form
        ...
--}}
<div class="page-amount-container" data-id="{{$controller_id}}"
@isset($show_header) style="display: none;" @endisset>
    <label class="page-amount">{{$strings_products['perpage']}}
        <select autocomplete="off" name="products_per_page" data-id="{{$controller_id}}">
            <option value="8" selected>8</option>
            <option value="12">12</option>
            <option value="16">16</option>
            <option value="show_all_products">{{$strings_products['all']}}</option>
        </select>
    </label>
</div>
<div class="paginator-container">
    @include('partials.component-pagination-numeric', ['controller_id' => $controller_id])
</div>
