# Pagination features:
+ more paginations on one browser page
+ select amout of items on one page
+ next/prev + numeric controlls
+ slide animations
- ajax loading (Not supported yet - because SEO needs all data within 5sec)
- lack of generic (its supported to 8-12-16-all only now)

Dependency: jQuery, jquery.effects.core.js (remove only aniamtion in .js to undepend)
Recommended plugin: Laravel Blade engine
---
## How to implement
1. Wrap pagination controllers elements with `<ul data-id="{{$controller_id}}">`
2. Identify prev/next/page with placeholders `data-page`: 
_possible options:(prev,next,{number})_
```
<li>
    <a href="javascript:void(0)" data-page="prev">
        Prev
    </a>
</li>
```
3. Include guide for pagination pairing:
    `@include('partials.component-pagination', ['controller_id' => 'id'])`
    'id' should be any string. That string should match 'id' in:
   ` @include('partials.component-{product/items/categ.}-list', ['controller_id' => 'id'])`
4. inlcude to blades everthing else

## Plans for improvement
1. add for loop with blade arg array(8,12,16,all). To generate select options and views in blade template.
2. Ajax with SEO.
3 ... what life brings on
