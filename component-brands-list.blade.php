<div class="row pagination-wraper"
 data-posts="{{ sizeof($items_results) }}"
 data-ifempty="{{ $strings_items['nothing_found'] }}"
 data-id="{{ $controller_id }}">
    <div class="div-8">
        @php($i = 0)
        @foreach( $items_results as $item )
            @php($i++)
            @if($i === 1 || $i % 8 === 1)
                <div class="row small-up-2 medium-up-3 large-up-4 items page
                @if($i > 1)
                    block-hidden
                @else
                    block-shown
                @endif
                item-8-{{intval($i / 8)}}">
            @endif
            {{-- $item start here --}}
                <div class="column column-block">
                    @php($image = get_field('image',$item))
                    <a href="{{get_term_link($item)}}" title="">
                        <div class="item">
                            <div class="column column-block text-center ">
                                <span class="helper" style=""></span>
                                <img src="{{$image['url']}}" alt="{{$image['alt']}}" title="{{$image['title']}}" />
                            </div>
                        </div>
                    </a>
                </div>
            {{-- $item end here --}}
            @if($i % 8 === 0)
            </div>
            @endif
        @endforeach
        @if($i % 8 != 0)
        </div>
        @endif
    </div>
    <div class="div-12">
            @php($i = 0)
            @foreach( $items_results as $item )
                @php($i++)
                @if($i === 1 || $i % 12 === 1)
                    <div class="row small-up-2 medium-up-3 large-up-4 items page
                    @if($i > 1)
                        block-hidden
                    @else
                        block-shown
                    @endif
                    item-12-{{intval($i / 12)}}">
                @endif
                <div class="column column-block">
                    @php($image = get_field('image',$item))
                    <a href="{{get_term_link($item)}}" title="">
                        <div class="item">
                            <div class="column column-block text-center ">
                                <span class="helper" style=""></span>
                                <img src="{{$image['url']}}" alt="{{$image['alt']}}" title="{{$image['title']}}" />
                            </div>
                        </div>
                    </a>
                </div>
                @if($i % 12 === 0)
                </div>
                @endif
            @endforeach
        @if($i % 12 != 0)
        </div>
        @endif
        </div>
        <div class="div-16">
            @php($i = 0)
            @foreach( $items_results as $item )
                @php($i++)
                @if($i === 1 || $i % 16 === 1)
                    <div class="row small-up-2 medium-up-3 large-up-4 items page
                    @if($i > 1)
                        block-hidden
                    @else
                        block-shown
                    @endif
                    item-16-{{intval($i / 16)}}">
                @endif
                <div class="column column-block">
                    @php($image = get_field('image',$item))
                    <a href="{{get_term_link($item)}}" title="">
                        <div class="item">
                            <div class="column column-block text-center ">
                                <span class="helper" style=""></span>
                                <img src="{{$image['url']}}" alt="{{$image['alt']}}" title="{{$image['title']}}" />
                            </div>
                        </div>
                    </a>
                </div>
                @if($i % 16 === 0)
                </div>
                @endif
            @endforeach
        @if($i % 16 != 0)
        </div>
        @endif
    </div>
    <div class="show_all_products">
        <div class="row small-up-2 medium-up-3 large-up-4 items page">
            @foreach( $items_results as $item )
            <div class="column column-block">
                @php($image = get_field('image',$item))
                <a href="{{get_term_link($item)}}" title="">
                    <div class="item">
                        <div class="column column-block text-center ">
                            <span class="helper" style=""></span>
                            <img src="{{$image['url']}}" alt="{{$image['alt']}}" title="{{$image['title']}}" />
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
